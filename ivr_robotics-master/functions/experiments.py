import ev3dev.ev3 as ev3
from time import sleep

gyro = ev3.GyroSensor
motorL = ev3.LargeMotor('outA')
motorR = ev3.LargeMotor('outD')

def calibrate_gyro(gyro):
    gyro.mode = 'GYRO-CAL'
    gyro.mode = 'GYRO-ANG'


def forward_motion_tests(percentage, time_s, write = True):
    gyro = ev3.GyroSensor()
    calibrate_gyro(gyro)
    motorR.position = 0
    motorL.position = 0

    speed = 9 * percentage # 900 * percentage / 100
    time  = time_s * 1000  # Convert to ms
    motorL.run_timed(speed_sp = speed, time_sp = time)
    motorR.run_timed(speed_sp = speed, time_sp = time)
    sleep(time_s)
    if write:
        file = open("../forward_motion_tests.txt", 'a')
        file.write("Forward motion test. Power - " + str(percentage) + "%. Time - " + str(time_s) + "s\n")
        file.write("Gyro Angle: " + str(gyro.value()) + '\n')
        file.write("Left Motor: " + str(motorL.position) + '\n')
        file.write("Right Motor: " + str(motorR.position) + "\n\n")
        file.close()
    else:
        print("Forward motion test. Power - " + str(percentage) + "%. Time - " + str(time_s) + "s\n")
        print("Gyro Angle: " + str(gyro.value()) + '\n')
        print("Left Motor: " + str(motorL.position) + '\n')
        print("Right Motor: " + str(motorR.position) + "\n\n")

    print("DONE!")

def rotation_tests(percentage, time_s, direction, write = True):
    gyro = ev3.GyroSensor()
    calibrate_gyro(gyro)
    motorR.position = 0
    motorL.position = 0
    speed = 9 * percentage # 900 * percentage / 100
    time  = time_s * 1000  # Convert to ms
    if direction == 'right':
        motorL.run_timed(speed_sp = speed, time_sp = time)

    elif direction == 'left':
        motorR.run_timed(speed_sp = speed, time_sp = time)
    sleep(time_s)
    if write:
        file = open("../rotation_tests.txt", 'a')
        file.write("Rotational motion test. Power - " + str(percentage) + "%. Time - " + str(time_s) + "s. Direction - " + direction + "\n\n")
        file.write("Gyro Angle: " + str(gyro.angle) + '\n')
        file.write("Left Motor: " + str(motorL.position) + '\n')
        file.write("Right Motor: " + str(motorR.position) + "\n\n")
        file.close()
    else:
        print("Rotational motion test. Power - " + str(percentage) + "%. Time - " + str(time_s) + "s. Direction - " + direction + "\n\n")
        print("Gyro Angle: " + str(gyro.angle) + '\n')
        print("Left Motor: " + str(motorL.position) + '\n')
        print("Right Motor: " + str(motorR.position) + "\n\n")
    print("DONE!")