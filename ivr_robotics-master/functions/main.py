#! /usr/bin/env python
# Core imports
import time
import ev3dev.ev3 as ev3

# Local Imports
#from functions import tutorial
# import functions.utilities
# import functions.openLoopControl as olc
# import functions.experiments as experiments
from time import sleep
print ('Welcome to ev3')
WHEEL_DIAMETER = 5.6

SPEED = 180

WHEEL_CIRCUMFERENCE = 2*3.14*WHEEL_DIAMETER
def cm_to_degrees(cm):
    angles_to_rotate = cm/WHEEL_DIAMETER*360
    if angles_to_rotate > 900:
        angles_to_rotate = 900
    elif angles_to_rotate < -900:
        angles_to_rotate = -900
    return angles_to_rotate




def convert_percentage(correction, default_speed, direction):
    if direction == 'right':
        turnL = default_speed + correction
        turnR = default_speed - correction
    else: # if direction == 'left':
        turnL = default_speed - correction
        turnR = default_speed + correction

    if turnR > 100:
        turnR = 100
    elif turnR < -100:
        turnR = -100
    if turnL > 100:
        turnL = 100
    elif turnL < -100:
        turnL = -100
    turnL *= 9
    turnR *= 9
    return turnR, turnL

def calculate_speed(correction):
    slower = SPEED - correction
    faster = SPEED + correction
    if faster > 300:
        faster = 300
    elif faster < -300:
        faster = -300
    if slower < -300:
        slower = -300
    elif slower > 300:
        slower = 300
    return slower, faster

def follow_wall(direction):

    motorL = ev3.LargeMotor('outA')
    motorL.position = 0
    motorR = ev3.LargeMotor('outD')
    motorR.position = 0
    usSensor = ev3.UltrasonicSensor()
    usSensor.mode = 'US-DIST-CM'
    goal = 100
    last_error = 0
    integral_error = 0
    k_P = 2
    k_P_original = k_P
    k_I = 0.00000
    k_D = 0.3

    at_edge = []

    while True:
        us_reading = usSensor.value()

        error_iter = goal - us_reading
        integral_error += error_iter
        derivative_error = error_iter - last_error

        correction = k_P * error_iter + k_I * integral_error + k_D * derivative_error
        slower, faster = calculate_speed(correction)
        print("Prop " + str(error_iter))
        print("Int: " + str(integral_error))
        print("Der: " + str(derivative_error))

        if direction == 'right':
            motorL.run_forever(speed_sp = faster)
            motorR.run_forever(speed_sp = slower)
        elif direction == 'left':
            motorL.run_forever(speed_sp = slower)
            motorR.run_forever(speed_sp = faster)


        if ev3.Button().down:
            motorL.stop()
            motorR.stop()
            exit(0)

#ev3.Sound.speak('Welcome to ev3').wait()

# Step A: Basic open driving
#tutorial.operateWheelsBasic()

# Step B: Turn on an off an LED using a switch
#tutorial.makeLightSwitch()

# Step C: Use switches to drive robot back and forward
#tutorial.makeLightAndMotorSwitch()


# Step D: Use a class to develop a bigger program with a state
#o = olc.openLoopControl()
## execute (with default params)
#o.operateWheels()
#
## update parameters
#o.time_to_spin = 1.0
#o.duty_cycle_sp = 50
#
## execute again
#o.operateWheels()


#motor_left = ev3.LargeMotor('outA')
#motor_right = ev3.LargeMotor('outD')
#
#motor_left.run_direct(speed_sp=75)
#motor_right.run_direct(speed_sp=75)


# Step E: Record values from the ultrasonic to a text file
#tutorial.recordUltraSonic()


#tutorial.recordColor()


#center = tutorial.findCenterOfObject(75, 20, 120, 30)
#tutorial.turnToObject(center,50)
#tutorial.turnToObject_PID(center,0.2,0,0)
follow_wall('left')

# LINE FOLLOWING
# kp - 0.8; ki = 0;
# reflectance = tutorial.calibrate_middle()
# tutorial.follow_line_PID(reflectance, 0.5, 0, 0.4)


# SENSOR EXPERIMENTAL DATA
# experiments.forward_motion_tests(25, 2)
# sleep(5)
# experiments.forward_motion_tests(50, 4)
# sleep(5)
# experiments.rotation_tests(25, 2, 'left')
# sleep(5)
# experiments.rotation_tests(25, 2, 'right')


# DATA GATHERING FOR PID VALUES
# reflectance = tutorial.calibrate_middle()
# reflectance = 50 # Consistent calibration proved problematic

# button = ev3.Button()
#
# ki = 0.005
# kp = 0.41
# kd = 0.4

# reflectance = 50
#
# kp = 0.285
# ki = 0.003
# kd = 0.3

# tutorial.follow_line_PID(reflectance, kp, ki, kd)

# while True:
    # kp += 0.01
    # enter_pressed = False
    # ki -= 0.0
    # kd -= 0.0
    # if kd < 1:
    #     break
    # if button.right:
    #     exit(0)
    #
    # while not enter_pressed:
    #     if button.enter:
    #         print('KD: ' + str(kd))
    #         enter_pressed = True
    #     if button.right:
    #         exit(0)

# tutorial.follow_line_middle()

#tutorial.experiments_motor(2000,75)

#tutorial.experiments_motor_speed_sp(4000,40)

#tutorial.calibrate_white_black()

#tutorial.calibrate_middle()

# remove this if you want it to exit as soon as its done:
#print "wait 10sec, then end"
#time.sleep(10)
