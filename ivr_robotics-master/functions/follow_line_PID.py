import ev3dev.ev3 as ev3
import time

SPEED = 20 # %
STOPPING_DISTANCE = 100 # mm

def follow_line_PID( k_P, k_I, k_D, direction = 'left', white_sensitivity = 40, object_detection = False):

    average_reflectance = 50

    # ev3.Sound().speak("Following Line")
    usSensor = ev3.UltrasonicSensor()
    usSensor.mode = 'US-DIST-CM'
    colSensor = ev3.ColorSensor()
    colSensor.mode = 'COL-REFLECT'
    motorL = ev3.LargeMotor('outA')
    motorR = ev3.LargeMotor('outD')
    goal = average_reflectance

    no_line = []
    last_error = 0
    integral_error = 0
    btn = ev3.Button()

    while True:

        col_reading = colSensor.value()
        error_iter = goal - col_reading
        integral_error += error_iter
        derivative_error = error_iter - last_error

        correction = k_P * error_iter + k_I * integral_error + k_D * derivative_error

        turnR, turnL = convert_percentage(correction, SPEED, direction)
        motorL.run_forever(speed_sp = turnL)
        motorR.run_forever(speed_sp = turnR)

        if col_reading > 75:
            no_line.append(col_reading)
            if len(no_line) == white_sensitivity: # After how many occurrences will we consider the line finished
                # ev3.Sound().speak("Line finished.")
                # time.sleep(2)
                return
        else:
            no_line = []

        if object_detection and usSensor.value() <= STOPPING_DISTANCE:
            return 'object'

        last_error = error_iter


        # Fail-safes
        if btn.down:
            exit(0)

        if btn.backspace:
            motorL.stop()
            motorR.stop()
            return

def convert_percentage(correction, default_speed, direction):
    if direction == 'right':
        turnL = default_speed + correction
        turnR = default_speed - correction
    else: # if direction == 'left':
        turnL = default_speed - correction
        turnR = default_speed + correction

    if turnR > 100:
        turnR = 100
    elif turnR < -100:
        turnR = -100
    if turnL > 100:
        turnL = 100
    elif turnL < -100:
        turnL = -100
    turnL *= 9
    turnR *= 9
    return turnR, turnL



def try_different_values(increment_kp,  increment_ki, increment_kd, limit_kp, limit_ki, limit_kd, kp, ki, kd):
    button = ev3.Button()

    reflectance = 50
    exited = False
    choice_made = False
    while True:
        print('What to increment?')
        while not choice_made:

            if button.left:
                increase_kp = True
                increase_kd = False
                increase_ki = False
                exited = False
                choice_made = True
                time.sleep(2)
            elif button.up:
                increase_ki = True
                increase_kp = False
                increase_kd = False
                exited = False
                choice_made = True
                time.sleep(2)
            elif button.right:
                increase_kd = True
                increase_ki = False
                increase_kp = False
                exited = False
                choice_made = True
                time.sleep(2)
            elif button.down:
                exit(0)
            else:
                pass

        while not exited:
            print("---")
            print("New iteration: ")
            print('KP: ' + str(kp))
            print('KI: ' + str(ki))
            print('KD: ' + str(kd))

            follow_line_PID(kp, ki, kd, reflectance)

            if increase_kp:
                kp += increment_kp
            elif increase_ki:
                ki += increment_ki
            elif increase_kd:
                kd += increment_kd

            enter_pressed = False

            if kp < limit_kp:
                break
            if ki < limit_ki:
                break
            if kd < limit_kd:
                break

            if button.right:
                exited = True

            while not enter_pressed:
                if button.enter:
                    enter_pressed = True
                if button.right:
                    exited = True
                    break
