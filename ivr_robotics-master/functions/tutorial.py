# Some simple open loop scripts 
from __future__ import division

import ev3dev.ev3 as ev3
from time import sleep
import functions.utilities as util

# duty cycle buggy because of upgraded kernel maybe - sometimes works, sometimes doesn't - use speed_sp and run_forever
# https://sites.google.com/site/ev3python/learn_ev3_python/using-motors

WHEEL_DIAMETER = 5.6
WHEEL_CIRCUMFERENCE = 2*3.14*WHEEL_DIAMETER

def operateWheelsBasic():
    print("spin the wheels")
    motor =ev3.LargeMotor('outA')
    motor2 = ev3.LargeMotor('outD')
    motor.connected
    # run_time takes milliseconds
    motor.run_timed(duty_cycle_sp = 100, time_sp=500)
    motor2.run_timed(speed_sp = 100, time_sp=500)
    sleep(1)
    motor.run_timed(speed_sp = -100, time_sp=500)
    motor2.run_timed(speed_sp = -100, time_sp=500)

    print('sleeping for 1 second')
    sleep(1)

def makeLightSwitch():
    print ("turn on LED w switch")
    print ("for 10 seconds")

    t_start = util.timestamp_now()
    ts = ev3.TouchSensor(ev3.INPUT_1)
    while True:
        ev3.Leds.set_color(ev3.Leds.LEFT, (ev3.Leds.GREEN, ev3.Leds.RED)[ts.value()])
        t_now = util.timestamp_now()
        if (t_now - t_start > 10E3):
            print ("finishing")
            break

    print ("turning off light")
    print (" ")
    ev3.Leds.set_color(ev3.Leds.LEFT, (ev3.Leds.GREEN, ev3.Leds.RED)[0])


def makeLightAndMotorSwitch():
    print ("drive forward and back")
    print ("using switches")
    print ("for 30 seconds")

    motor = ev3.LargeMotor('outA')
    motor.connected

    t_start = util.timestamp_now()
    ts = ev3.TouchSensor()
    ts2 = ev3.TouchSensor(ev3.INPUT_2)
    while True:
        ev3.Leds.set_color(ev3.Leds.LEFT, (ev3.Leds.GREEN, ev3.Leds.RED)[ts.value()])
        ev3.Leds.set_color(ev3.Leds.RIGHT, (ev3.Leds.GREEN, ev3.Leds.RED)[ts2.value()])

        if (ts.value()):
            motor.run_timed(duty_cycle_sp=100, time_sp=50)
        elif (ts2.value()):
            motor.run_timed(duty_cycle_sp=-100, time_sp=50)

        t_now = util.timestamp_now()
        if (t_now - t_start > 30E3):
            print ("im done")
            break

    print ("turning off light, done")
    ev3.Leds.set_color(ev3.Leds.LEFT, (ev3.Leds.GREEN, ev3.Leds.RED)[0])


def recordUltraSonic():
    print("Record readings from ultrasonic")
    print("Will print after back button is pressed")

    btn = ev3.Button()

    sonar = ev3.UltrasonicSensor(ev3.INPUT_1)
    sonar.connected
    sonar.mode = 'US-DIST-CM' # will return value in mm

    readings = ""
    readings_file = open('./ivr_robotics-master/results.txt', 'w')

    while not btn.backspace:
        readings = readings + str(sonar.value()) + '\n'
    readings_file.write(readings)
    readings_file.close() # Will write to a text file in a column


# records color - values for the color sensor mode 'COL-COLOR' are 0-7 - may not be good enough.
def recordColor():
    print("Record readings from color")
    print("Will print after back button is pressed")

    btn = ev3.Button()

    color_det = ev3.ColorSensor(ev3.INPUT_4)
    color_det.connected
    color_det.mode = 'COL-COLOR'

    readings = ""
    readings_file = open('./sensor_reads/colorReadings2.txt', 'w')

    while not btn.backspace:
        readings = readings + str(color_det.value()) + '\n'
    readings_file.write(readings)
    readings_file.close() # Will write to a text file in a column


# Not very useful

# def moveServo():
#
#     servo = ev3.MediumMotor('outB')
#     #while True:
#     #servo.run_forever()
#     # print("Position is: " + str(servo.position))
#     # servo.run_to_rel_pos( duty_cycle_sp = 40, position_sp = 125)
#     # time.sleep(10)
#     # print("Position is: " + str(servo.position))
#     # servo.run_to_rel_pos( duty_cycle_sp = -40, position_sp = -250)
#     # time.sleep(10)
#     # print("Position is: " + str(servo.position))
#     # servo.run_to_rel_pos( duty_cycle_sp = 40, position_sp = 62)
#
#     # servo.run_timed(time_sp=2000,speed_sp= 30)
#     #
#     # servo.run_timed(time_sp=2000,speed_sp=-30)
#     #
#     #
#     # servo.run_timed(time_sp=2000,speed_sp=-25)
#     # time.sleep(1)
#     # servo.run_timed(time_sp=2000,speed_sp= 30)
#
#     while True:
#
#         servo.run_timed(speed_sp=-75, time_sp=1000)
#
#         sleep(1)
#
#         servo.run_timed(speed_sp=75, time_sp=1000)
#
#         sleep(1)


# Find the edge of an object
def moveServoWithObjectFind(servo_speed, object_range, rotation_limit):

    servo = ev3.MediumMotor('outB')
    servo.run_to_abs_pos(position_sp=0) # reset servo at starting position
    sleep(2)    # give time for the servo to go to the starting pos
    servo.run_forever(speed_sp = servo_speed)    # speed_sp = angle per second
    sonar = ev3.UltrasonicSensor(ev3.INPUT_1)

    while True:
        pos = servo.position    # position in angles from -9000 to 9000 or sth not sure
        #print("Sonar orientation: " + str(servo.position))
        #print("Sonar reading: " + str(sonar.value()))
        if sonar.value()/10 <= object_range:  # detect object within object_range centimeters
            object_pos = servo.position
            #print("Object at angle: " + str(object_pos))
            return object_pos, servo, sonar
        elif pos > rotation_limit:  # if not found switch direction of rotation
            servo.run_forever(speed_sp = -servo_speed)
        elif pos < -rotation_limit:
            servo.run_forever(speed_sp = servo_speed)


# Finds the center of the object
def findCenterOfObject(servo_speed, object_range, rotation_limit, object_farthest):

    objectEdge, servo, sonar = moveServoWithObjectFind(servo_speed, object_range, rotation_limit)
    if objectEdge > 0:
        servo.run_forever(speed_sp=servo_speed)
    elif objectEdge < 0:
        servo.run_forever(speed_sp = -servo_speed)

    while True:
        if sonar.value()/10 > object_farthest: # if the detected value is larger than the farthers possible point for the object
            other_edge = servo.position
            servo.stop()
            center = (objectEdge + other_edge)/2
            servo.run_to_abs_pos(position_sp=0) # reset servo
            sleep(4)
            print("Center of object: " + str(center))
            return center

# This works sometimes and sometimes it doesn't - duty_cycle is buggy af (exercise from the lab).
def experiments_motor(time,power):

    motor1 = ev3.LargeMotor('outA')
    motor2 = ev3.LargeMotor('outD')
    gyro = ev3.GyroSensor()
    print("Position m1 before: " + str(motor1.position))
    print("Position m2 before: " + str(motor2.position))
    print("Gyro reading before: " + str(gyro.value()))
    motor1.run_timed(duty_cycle_sp = power, time_sp = time)
    motor2.run_timed(duty_cycle_sp = power, time_sp = time)

    sleep(time/1000+2)
    print("Position m1 after: " + str(motor1.position))
    print("Position m2 after: " + str(motor2.position))
    print("Gyro reading after: " + str(gyro.value()))
    motor1.stop()
    motor2.stop()

# don't use run_direct with speed_sp argument - the motor bugs
# use run_direct with duty_cycle_sp argument
# time_sp on run_direct makes no effect


# Same as above but uses speed_sp instead of the duty_cycle.
def experiments_motor_speed_sp(time,power):

    motor1 = ev3.LargeMotor('outA')
    motor2 = ev3.LargeMotor('outD')
    gyro = ev3.GyroSensor()
    print("Position m1 before: " + str(motor1.position))
    print("Position m2 before: " + str(motor2.position))
    print("Gyro reading before: " + str(gyro.value()))
    motor1.run_timed(speed_sp = power, time_sp = time)
    motor2.run_timed(speed_sp = power, time_sp = time)

    sleep(time/1000 + 2)
    print("Position m1 after: " + str(motor1.position))
    print("Position m2 after: " + str(motor2.position))
    print("Gyro reading after: " + str(gyro.value()))

    motor1.stop()
    motor2.stop()
    return


def turnToObject(centerObject, turnSpeed):

    gyro = ev3.GyroSensor()
    gyro.mode = 'GYRO-CAL'  # calibrate sensor - set gyro.value() = 0
    gyro.mode = 'GYRO-ANG'
    motor1 = ev3.LargeMotor('outA')
    motor2 = ev3.LargeMotor('outD')

    if centerObject > 0:
        goal_orientation = centerObject - 25    # -25 because measurements between the sonar and the gyro are kinda off.
                                                # we may need to adjust it so that is not 25 but it depends on how far the object is from the sensor.
                                                # Try to derive some relationship.
        print("Goal orientation: " + str(goal_orientation))
        motor1.run_forever(speed_sp = turnSpeed)
        motor2.run_forever(speed_sp = -turnSpeed)
    elif centerObject < 0:
        goal_orientation = centerObject + 25
        print("Goal orientation: " + str(goal_orientation))
        motor1.run_forever(speed_sp = -turnSpeed)
        motor2.run_forever(speed_sp = turnSpeed)
    turned = False
    while not turned:
        #print(gyro.value())
        if abs(gyro.value() - goal_orientation) < 5:
            motor1.stop()
            motor2.stop()
            turned = True

            print("Approximate turned value: " + str(gyro.value()))
    sleep(4)


# Same as above but with PID - check this http://thetechnicgear.com/2014/03/howto-create-line-following-robot-using-mindstorms/
def turnToObject_PID(centerObject, k_P, k_I, k_D):

    gyro = ev3.GyroSensor()
    motor1 = ev3.LargeMotor('outA')
    motor2 = ev3.LargeMotor('outD')
    gyro.mode = 'GYRO-CAL'
    gyro.mode='GYRO-ANG'

    if centerObject < 0:
        goal_orientation = centerObject + 25
    elif centerObject > 0:
        goal_orientation = centerObject - 25


    print("Initial orientation: " + str(gyro.value()))
    print("Goal orientation: " + str(goal_orientation))
    last_error = 0
    integral_error = 0

    while True:
        gyro_reading = gyro.value()

        if abs(gyro_reading - goal_orientation) < 5:
            return
        error_iter = goal_orientation - gyro_reading
        integral_error += error_iter
        derivative_error = error_iter - last_error

        correction = k_P*error_iter + k_I*integral_error + k_D*derivative_error
        #speed =  turnSpeed*correction
        print("Proportional error: "+ str(error_iter))
        print("Integral error: " + str(integral_error))
        print("Derivatrive error: " + str(derivative_error))
        print("Correction: " + str(correction))
        #print("Speed: " + str(speed))

        motor1.run_forever(speed_sp = correction)
        motor2.run_forever(speed_sp = -correction)

def calibrate_white_black():
    sensor = ev3.ColorSensor()
    sensor.mode = 'COL-REFLECT'     # 2 modes for light detection - 'COL-REFLECT' - % of reflected light
    black_calibrated = False
    white_calibrated = False

    button = ev3.Button()
    while True:
        if button.backspace and not black_calibrated:
            print("Calibrating black.")
            black = sensor.value()
            black_calibrated = True
            print("Sleeping for 3 seconds.")
            sleep(3)
        if button.backspace and black_calibrated and not white_calibrated:
            print("Calibrating white.")
            white = sensor.value()
            white_calibrated = True
            print("Sleeping for 2 seconds.")
            sleep(2)
        if black_calibrated and white_calibrated:
            print("Black value(percentage reflected light): " + str(black))
            print("White value: " + str(white))
            return black, white

def calibrate_middle():
    sensor = ev3.ColorSensor()
    sensor.mode = 'COL-REFLECT'
    bckspace_pressed = False

    button = ev3.Button()

    while not bckspace_pressed:
        if button.backspace:
            middle = sensor.value()
            print("Calibrating middle.")
            bckspace_pressed = True


    print("Middle value: " + str(middle))
    return middle

# Convert cm to angles - 900 <= speed_sp <= 900
def cm_to_degrees(cm):
    angles_to_rotate = cm/WHEEL_DIAMETER*360
    if angles_to_rotate > 900:
        angles_to_rotate = 900
    elif angles_to_rotate < -900:
        angles_to_rotate = -900
    return angles_to_rotate

# Go forward within distance 'distance' of object.
def goTowardsObject_PID(distance, k_P, k_I, k_D):

    usSensor = ev3.UltrasonicSensor()
    usSensor.mode = 'US-DIST-CM'
    motor1 = ev3.LargeMotor('outA')
    motor2 = ev3.LargeMotor('outD')
    goal = distance

    print("Initial distance: " + str(usSensor.value()))
    print("Goal distance: " + str(goal))
    last_error = 0
    integral_error = 0

    while True:
        us_reading = usSensor.value()/10
        error_iter = us_reading - goal
        integral_error += error_iter
        derivative_error = error_iter - last_error

        correction = k_P*error_iter + k_I*integral_error + k_D*derivative_error
        #speed =  turnSpeed*correction
        print("Proportional error: "+ str(error_iter))
        print("Integral error: " + str(integral_error))
        print("Derivative error: " + str(derivative_error))
        print("Correction: " + str(correction))
        #print("Speed: " + str(speed))

        angles_to_rotate = cm_to_degrees(correction)
        motor1.run_forever(speed_sp = angles_to_rotate)
        motor2.run_forever(speed_sp = angles_to_rotate)


def follow_wall_PID(distance, k_P, k_I, k_D):

    usSensor = ev3.UltrasonicSensor()
    usSensor.mode = 'US-DIST-CM'
    motor1 = ev3.LargeMotor('outA')
    motor2 = ev3.LargeMotor('outD')
    goal = distance

    print("Initial distance: " + str(usSensor.value()))
    print("Goal distance: " + str(goal))
    last_error = 0
    integral_error = 0

    while True:
        us_reading = usSensor.value()/10
        error_iter = us_reading - goal
        integral_error += error_iter
        derivative_error = error_iter - last_error

        correction = k_P*error_iter + k_I*integral_error + k_D*derivative_error
        #speed =  turnSpeed*correction
        print("Proportional error: "+ str(error_iter))
        print("Integral error: " + str(integral_error))
        print("Derivative error: " + str(derivative_error))
        print("Correction: " + str(correction))
        #print("Speed: " + str(speed))

        angles_to_rotate = cm_to_degrees(correction)
        motor1.run_forever(speed_sp = angles_to_rotate)
        motor2.run_forever(speed_sp = angles_to_rotate)


def follow_line_PID(average_reflectance, k_P, k_I, k_D, direction = 'left'):
    print("Following Line")
    colSensor = ev3.ColorSensor()
    colSensor.mode = 'COL-REFLECT'
    motorL = ev3.LargeMotor('outA')
    motorR = ev3.LargeMotor('outD')
    goal = average_reflectance

    # print("Initial distance: " + str(colSensor.value()))
    # print("Goal distance: " + str(goal))
    no_line = []
    last_error = 0
    integral_error = 0
    btn = ev3.Button()

    while True:

        col_reading = colSensor.value()
        error_iter = goal - col_reading
        integral_error += error_iter
        derivative_error = error_iter - last_error

        correction = k_P * error_iter + k_I * integral_error + k_D * derivative_error

        # speed =  turnSpeed*correction
        # print("Proportional error: " + str(error_iter))
        # print("Integral error: " + str(integral_error))
        # print("Derivative error: " + str(derivative_error))
        # print("Correction: " + str(correction))
        # print("Speed: " + str(speed))

        turnR, turnL = col_difference_to_rotation(correction, 20, direction)
        motorL.run_forever(speed_sp = turnL)
        motorR.run_forever(speed_sp = turnR)

        if col_reading > 75:
            no_line.append(col_reading)
            if len(no_line) == 40:
                return
        else:
            no_line = []

        if btn.right:
            exit(0)

        if btn.backspace:
            motorL.stop()
            motorR.stop()
            return
        last_error = error_iter


# How to map color difference to motor adjustment?
def col_difference_to_rotation(correction, default_speed, direction):
    if direction == 'right':
        turnL = default_speed + correction
        turnR = default_speed - correction
    else: # if direction == 'left':
        turnL = default_speed - correction
        turnR = default_speed + correction

    if turnR > 100:
        turnR = 100
    elif turnR < -100:
        turnR = -100
    if turnL > 100:
        turnL = 100
    elif turnL < -100:
        turnL = -100
    turnL *= 9
    turnR *= 9
    return turnR, turnL


def go_forward(motor1, motorR, speed):
    motor1.run_forever(speed_sp = speed)
    motorR.run_forever(speed_sp = speed)



def follow_line_middle():
    black, white = calibrate_white_black()

    colSensor = ev3.ColorSensor()

    # Value is 1 for black and 6 for white
    colSensor.mode = 'COL-COLOR'
    motorL = ev3.LargeMotor('outA')
    motorR = ev3.LargeMotor('outD')

    speed = 200
    offset = 0

    turned_left = False

    while True:
        color = colSensor.value()
        if color == black:
            go_forward(motorL, motorR, speed)
        elif turned_left:
            if offset != 0:
                while colSensor.value() != black:
                    motorL.run_forever(speed_sp=speed - offset)
                    motorR.run_forever(speed_sp=speed)
            while offset < speed / 2 or colSensor.value() == black:
                motorL.run_forever(speed_sp=speed - offset)
                motorR.run_forever(speed_sp=speed)
                offset += 1
            if offset >= speed:
                turned_left = False
            else:
                turned_left = False
                offset = 0
        elif not turned_left:
            if offset != 0:
                while colSensor.value() != black:
                    motorL.run_forever(speed_sp=speed)
                    motorR.run_forever(speed_sp=speed - offset)
            while offset < speed / 2 or colSensor.value() == black:
                motorL.run_forever(speed_sp=speed)
                motorR.run_forever(speed_sp=speed - offset)
                offset += 1
            if offset >= speed:
                turned_left = True
            else:
                turned_left = True
                offset = 0




# pseudocode line following easy(not following the line edge but the middle of the line)
#
#     calibrate white and black
#     put robot in middle of line
#     turned left = false
#     while True
#         if color = black
#             go forward
#         else if not turned left
#             turn left
#             turned left = true
#         else if turned left
#             turn right
#             turned left = false

