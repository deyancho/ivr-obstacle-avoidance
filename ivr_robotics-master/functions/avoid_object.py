from time import sleep
import ev3dev.ev3 as ev3
from functions.follow_line_PID import follow_line_PID
import time

#Constants
WHEEL_DIAMETER = 5.6
TIME_TO_OVERSHOOT_LINE = 200
ROBOT_SIZE = 3 * WHEEL_DIAMETER # A bit over the realistic size
ROBOT_SIZE_DEG = 3 * 360 # ~3 turns of the wheels
TURN_SPEED = 180 # .5 rps
COL_CALIBRATION = 50
SPEED = 180

# kp = 0.3
# ki = 0.003
# kd = 0.26

kp = 0.29
ki = 0.0045
kd = 0.26



def avoid_obstacles():

    direction = select_direction()

    gyro = ev3.GyroSensor()
    gyro.mode = 'GYRO-CAL'
    gyro.mode = 'GYRO-ANG'

    motorL = ev3.LargeMotor('outA')
    motorR = ev3.LargeMotor('outD')

    usSensorCtrl = ev3.MediumMotor('outB')
    usSensorCtrl.position = 0
    #usSensorCtrl.run_to_abs_pos(speed_sp = 180, position_sp=0)

    usSensor = ev3.UltrasonicSensor()
    usSensor.mode = 'US-DIST-CM'

    colSensor = ev3.ColorSensor()
    colSensor.mode = 'COL-REFLECT'

    while True:
        ev3.Sound.speak("Following line")
        sleep(2)
        if follow_line_PID(kp, ki, kd, direction, object_detection=True, white_sensitivity=500) == 'object':
            # The following if should be checked on each tick of the follow_line_PID loop
            ev3.Sound().speak("Obstacle detected")
            sleep(2)
            avoidance_strategy(motorL, motorR, usSensor, usSensorCtrl, colSensor, gyro, direction)
            usSensorCtrl.run_to_abs_pos(speed_sp = 180, position_sp=0)
            sleep(1)
        else:
            break


def avoidance_strategy(motorL, motorR, usSensor, usSensorCtrl, colSensor, gyro, direction):
    motorL.stop()
    motorR.stop()

    direction = look_around(usSensor, usSensorCtrl)
    # usSensorCtrl.run_to_abs_pos(speed_sp  = 180, position_sp = 100) # received from look_around


    if direction == 'left':
        turn(motorL, motorR, gyro, True)
        ev3.Sound().speak("Following wall")
        sleep(2)
        go_around_PID(direction, usSensor, colSensor, gyro, motorL, motorR)
    else:
        turn(motorL, motorR, gyro, False)
        go_around_PID(direction, usSensor, colSensor, gyro, motorL, motorR)


def look_around(usSensor, usSensorCtrl):
    # usSensorCtrl.run_to_abs_pos(speed_sp = 180, position_sp = 90) #Right
    # sleep(2)
    # right = usSensor.value()
    # print("Looked RIGHT and saw: " + str(right))
    # usSensorCtrl.run_to_abs_pos(speed_sp = 180, position_sp = -90) #Left
    # sleep(2)
    # left = usSensor.value()
    # print("Looked LEFT and saw: " + str(left))
    # if max(right,left) == right:
    #     print("Going RIGHT")
    usSensorCtrl.run_to_abs_pos(speed_sp  = 180, position_sp = 100)
    return 'left'
    # else:
    #     print("Looking LEFT")
    #     usSensorCtrl.run_to_abs_pos(speed_sp  = 180, position_sp = 100)
    #     return 'left'


def turn(motorL, motorR, gyro, left):
    angle = gyro.value()
    if left:
        print("TURNING 90 DEGREES LEFT")
        while gyro.value() > angle - 86:
            motorL.run_forever(speed_sp=-TURN_SPEED)
            motorR.run_forever(speed_sp=TURN_SPEED)
            if ev3.Button().down:
                exit(0)
    else:
        print("TURNING 90 DEGREES RIGHT")
        while gyro.value() < angle + 86:
            motorL.run_forever(speed_sp=TURN_SPEED)
            motorR.run_forever(speed_sp=-TURN_SPEED)
            if ev3.Button().down:
                exit(0)
    motorR.stop()
    motorL.stop()

def go_around_PID(direction, usSensor, colSensor, gyro, motorL, motorR):

    goal = 140
    last_error = 0
    integral_error = 0
    k_P = 0.2
    k_P_original = k_P
    k_I = 0
    k_D = 0

    at_edge = []

    while True:
        us_reading = usSensor.value()

        if colSensor.value() < 10:
            motorL.stop()
            motorR.stop()
            ev3.Sound().speak("Found line. Aligning.")
            sleep(2)
            converge_on_line(motorL, motorR, colSensor, direction)
            return

        if us_reading > 800:
            at_edge.append(us_reading)
            motorL.stop()
            motorR.stop()
            # if len(at_edge) == 5:
            #     # if overcome_edge(direction, motorL, motorR, gyro, usSensor, colSensor) == 'line':
            #     #     break
            #     k_P = 0.01
            #     at_edge = []
            #     # continue
            # else:
            #     continue
            # timeout = time.time() + 2
            angle = gyro.value()
            while gyro.value() < angle + 70:
                # motorL.run_timed(speed_sp=300, time_sp=2000)
                # motorR.run_timed(speed_sp=120, time_sp=2000)
                motorL.run_forever(speed_sp=300)
                motorR.run_forever(speed_sp=120)
            # while True:
                if colSensor.value() < 10:
                    motorL.stop()
                    motorR.stop()
                    ev3.Sound().speak("Found line. Aligning.")
                    sleep(1)
                    converge_on_line(motorL, motorR, colSensor, direction)
                    return
                # if time.time() >= timeout:
                #     break
                if usSensor.value() < goal + 20:
                    break
            if usSensor.value() > 300:
                if seek_wall(motorL, motorL, usSensor, colSensor) == 'line':
                    motorL.stop()
                    motorR.stop()
                    ev3.Sound().speak("Found line. Aligning.")
                    sleep(1)
                    converge_on_line(motorL, motorR, colSensor, direction)
                    return

            #sleep(2)

        else:
            at_edge = []
            k_P = k_P_original

        error_iter = goal - us_reading
        integral_error += error_iter
        derivative_error = error_iter - last_error

        correction = k_P * error_iter + k_I * integral_error + k_D * derivative_error
        slower, faster = calculate_speed(correction)


        if direction == 'right':
            motorL.run_forever(speed_sp = faster)
            motorR.run_forever(speed_sp = slower)
        elif direction == 'left':
            motorL.run_forever(speed_sp = slower)
            motorR.run_forever(speed_sp = faster)


        if ev3.Button().down:
            exit(0)

def calculate_speed(correction):
    slower = SPEED - correction
    faster = SPEED + correction
    if faster > 300:
        faster = 300
    elif faster < -300:
        faster = -300
    if slower < -300:
        slower = -300
    elif slower > 300:
        slower = 300
    return slower, faster


def select_direction():
    button = ev3.Button()
    print("Select direction to follow the line from.")
    while True:
        if button.left:
            return 'left'
        elif button.right:
            return 'right'


# def overcome_edge(direction, motorL, motorR, gyro, usSensor, colSensor):
#     if direction == 'left':
#         opposite = False
#     else:
#         opposite = True
#
#     move_forward(motorL, motorR,1)
#     # Can speak here
#     sleep(1)
#     turn(motorL, motorR, gyro, opposite)
#     # Can speak here
#     sleep(1)
#     return seek_wall(motorL, motorR, usSensor, colSensor)
#
#
# # Moves robot's size forward
# def move_forward(motorL, motorR, time_s):
#     print("Moving Forward")
#     time_ms = time_s * 1000
#     motorL.run_timed(speed_sp = SPEED, time_sp = time_ms)
#     motorR.run_timed(speed_sp = SPEED, time_sp = time_ms)
#     sleep(time_s)
#
#     # start = motorL.position
#     # while abs(motorL.position - start) < ROBOT_SIZE_DEG / 7: # / 7 adjusts for the PID's time to respond
#     #     motorL.run_forever(speed_sp=SPEED)
#     #     motorR.run_forever(speed_sp=SPEED)
#     #
#     #
#     #     if ev3.Button().down:
#     #         exit(0)
#
#
#
def seek_wall(motorL, motorR, usSensor, colSensor):
    false_positive = []
    while len(false_positive) < 3 and colSensor.value() > 10:
        if usSensor.value() > 300:
            false_positive.append(usSensor.value())
        motorL.run_forever(speed_sp=SPEED)
        motorR.run_forever(speed_sp=SPEED)
        if ev3.Button().down:
            exit(0)

    if colSensor.value() < 10:
        return 'line'
    else:
        return 'wall'

def converge_on_line(motorL, motorR, colSensor, direction):
    btn = ev3.Button()

    while colSensor.value() < 48:
        motorL.run_forever(speed_sp=180)
        motorR.run_forever(speed_sp=180)
        if btn.down:
            exit(0)


    motorL.run_timed(speed_sp=180, time_sp = TIME_TO_OVERSHOOT_LINE)
    motorR.run_timed(speed_sp=180, time_sp = TIME_TO_OVERSHOOT_LINE)
    sleep(0.2)
    motorL.stop()
    motorR.stop()

    if direction == 'right':
        while colSensor.value() > 48:
            # print("Gyro Value " + str(gyro.value()))

            motorL.run_forever(speed_sp = 180)
            if btn.down:
                exit(0)
    else:
        while colSensor.value() > 48:  # Slight offset
            # print("Gyro Value " + str(gyro.value()))
            motorR.run_forever(speed_sp = 180)
            if btn.down:
                exit(0)

    motorL.stop()
    motorR.stop()