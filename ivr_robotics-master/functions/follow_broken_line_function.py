from functions import follow_line_PID
from time import sleep
import ev3dev.ev3 as ev3

TIME_TO_OVERSHOOT_LINE = 200 #ms
WHITENESS_SENSITIVITY = 15

def follow_broken_line():
    s = ev3.Sound()

    buttons = ev3.Button()
    fst = select_direction()
    gyro = ev3.GyroSensor()
    gyro.mode = 'GYRO-CAL'
    gyro.mode = 'GYRO-ANG'

    motorL = ev3.LargeMotor('outA')
    motorR = ev3.LargeMotor('outD')

    colSensor = ev3.ColorSensor()

    if fst == 'left':
        snd = 'right'
    else:
        snd = 'left'

    kp = 0.3
    ki = 0.003
    kd = 0.26
    # 0.15,0.007,0.1
    line_count = 1
    s.speak("Following segment 1.")
    sleep(2)
    follow_line_PID.follow_line_PID(kp,ki,kd,fst, WHITENESS_SENSITIVITY)
    s.speak("Segment 1 has ended.")
    sleep(2)
    # k_P = 0.25
    # k_I = 0.005
    # k_D = 0.15
    while line_count < 4:
        # Turn
        str_to_speak = "Searching segment " + str(line_count + 1) + "."
        s.speak(str_to_speak)
        sleep(2)
        if fst == 'right':
            degrees_to_turn = gyro.value() - 65
            while gyro.value() > degrees_to_turn:
                motorR.run_forever(speed_sp = 50)
                motorL.run_forever(speed_sp = -50)
                if buttons.down:
                    exit(0)
        else:
            degrees_to_turn = gyro.value() + 65
            while gyro.value() < degrees_to_turn:
                motorL.run_forever(speed_sp = 50)
                motorR.run_forever(speed_sp = -50)
                if buttons.down:
                    exit(0)
        motorL.stop()
        motorR.stop()

        seek_line(motorL, motorR, colSensor, gyro, fst)
        str_to_speak = "Following segment " + str(line_count + 1) + "."
        s.speak(str_to_speak)
        sleep(2)
        if line_count == 4:
            follow_line_PID.follow_line_PID(kp,ki,kd,snd, 4)
        else:
            follow_line_PID.follow_line_PID(kp, ki, kd, snd, WHITENESS_SENSITIVITY)
        str_to_speak = "Segment " + str(line_count + 1) + " has ended."
        s.speak(str_to_speak)
        sleep(2)
        trd = fst
        fst = snd
        snd = trd
        line_count += 1

        if buttons.down:
            exit(0)
    s.speak("I'm done following lines.`")
    sleep(5)
    return

def seek_line(motorL, motorR, colSensor, gyro, direction):
    print("Seeking Line")
    btn = ev3.Button()
    while colSensor.value() > 48:
        motorL.run_forever(speed_sp = 180)
        motorR.run_forever(speed_sp = 180)
        if btn.down:
            exit(0)

    while colSensor.value() < 48:
        motorL.run_forever(speed_sp=180)
        motorR.run_forever(speed_sp=180)
        if btn.down:
            exit(0)

    ev3.Sound().speak('A segment was found!')

    motorL.run_timed(speed_sp=180, time_sp = TIME_TO_OVERSHOOT_LINE)
    motorR.run_timed(speed_sp=180, time_sp = TIME_TO_OVERSHOOT_LINE)
    # sleep(0.2)
    motorL.stop()
    motorR.stop()
    if direction == 'right':
        while colSensor.value() > 48:
            print("Gyro Value " + str(gyro.value()))

            motorL.run_forever(speed_sp = 180)
            if btn.down:
                exit(0)
    else:
        while colSensor.value() > 48:  # Slight offset
            print("Gyro Value " + str(gyro.value()))
            motorR.run_forever(speed_sp = 180)
            if btn.down:
                exit(0)


    motorL.stop()
    motorR.stop()

    print("Done seeking")


def select_direction():
    print('Select direction')
    buttons = ev3.Button()
    while True:
        if buttons.left:
            return 'left'
        elif buttons.right:
            return 'right'