#! /usr/bin/env python
from functions import follow_line_PID
import ev3dev.ev3 as ev3

#print("Choose a direction to follow the line from")
ev3.Sound.speak("Choose side of the track.")
button = ev3.Button()
direction = ''
while True:
    if button.left:
        direction = 'left'
        ev3.Sound.speak("I am to the left of the track.")
        break
    elif button.right:
        direction = 'right'
        ev3.Sound.speak("I am to the right of the track.")
        break


kp = 0.3
ki = 0.003
kd = 0.26

# kp = 0.29
# ki = 0.0045
# kd = 0.26


follow_line_PID.follow_line_PID(kp,ki,kd,direction, white_sensitivity=500)
#follow_line_PID.try_different_values(0.1,0.001,0.01,1,0.01,1,0.34,0.003,0.24)